#!/usr/pkg/bin/bash

TEST_DIR=/usr/mfs_test
mkdir $TEST_DIR

# Writing 0's from /dev/zero
time dd if=/dev/zero of=$TEST_DIR/test.out bs=4096 count=1000
time dd if=/dev/zero of=$TEST_DIR/test.out bs=4096 count=10000
time dd if=/dev/zero of=$TEST_DIR/test.out bs=4096 count=100000

# Writing random data from /dev/random
time dd if=/dev/random of=$TEST_DIR/test.out bs=4096 count=1000
time dd if=/dev/random of=$TEST_DIR/test.out bs=4096 count=10000
time dd if=/dev/random of=$TEST_DIR/test.out bs=4096 count=100000

rm $TEST_DIR/test.out

# Copying source code
time cp -R /usr/src/minix/fs $TEST_DIR
rm -rf "$TEST_DIR"/*
time cp -R /usr/src/minix $TEST_DIR
rm -rf "$TEST_DIR"/*
time cp -R /usr/src $TEST_DIR
rm -rf "$TEST_DIR"/*

VIDEO_DIR=/usr/videos
# Copying video files
time cp $VIDEO_DIR/Superman.mp4 $TEST_DIR/video
time cp $VIDEO_DIR/TheGoldRush.mkv $TEST_DIR/video
rm $TEST_DIR/video
